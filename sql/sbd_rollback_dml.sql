-- #################################################################
--
-- Parameters:
--   1: DB SID
--   2: Owner user
--   3: Owner password
--
-- Usage:
--   sqlplus /nolog @sbd_install_ddl.sql <ORACLE_SID> <PRODUCT_USER> <PRODUCT_PASS>
--
--
--#################################################################
--
SET DEFINE ON
--
DEFINE db_sid  = &1
DEFINE own_usr = &2
DEFINE own_pwd = &3
--
CONNECT &own_usr/&own_pwd@&db_sid
--
--
SET FEEDBACK OFF
--
SET FEEDBACK ON
--

--
-- LISTA_TRANSFEROW --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'LISTA_TRANSFEROW';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE LISTA_TRANSFEROW';
    END IF;
END;
/
--
PROMPT
PROMPT *** LISTA_TRANSFEROW table has been dropped
PROMPT
--

--
-- TRANSFER --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'TRANSFER';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE TRANSFER';
    END IF;
END;
/
--
PROMPT
PROMPT *** TRANSFER table has been dropped
PROMPT
--

--
-- GOL --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'GOL';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE GOL';
    END IF;
END;
/
--
PROMPT
PROMPT *** GOL table has been dropped
PROMPT
--

--
-- KARTKA --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'KARTKA';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE KARTKA';
    END IF;
END;
/
--
PROMPT
PROMPT *** KARTKA table has been dropped
PROMPT
--

--
-- ZAWODNIK --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'ZAWODNIK';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE ZAWODNIK';
    END IF;
END;
/
--
PROMPT
PROMPT *** ZAWODNIK table has been dropped
PROMPT
--

--
-- LISTA_SEDZIOW --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'LISTA_SEDZIOW';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE LISTA_SEDZIOW';
    END IF;
END;
/
--
PROMPT
PROMPT *** LISTA_SEDZIOW table has been dropped
PROMPT
--

--
-- SEDZIA --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'SEDZIA';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE SEDZIA';
    END IF;
END;
/
--
PROMPT
PROMPT *** SEDZIA table has been dropped
PROMPT
--

--
-- MECZ --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'MECZ';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE MECZ';
    END IF;
END;
/
--
PROMPT
PROMPT *** MECZ table has been dropped
PROMPT
--

--
-- KOLEJKA --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'KOLEJKA';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE KOLEJKA';
    END IF;
END;
/
--
PROMPT
PROMPT *** KOLEJKA table has been dropped
PROMPT
--

--
-- DRUZYNA --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'DRUZYNA';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE DRUZYNA';
    END IF;
END;
/
--
PROMPT
PROMPT *** DRUZYNA table has been dropped
PROMPT
--

--
-- TRENER --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'TRENER';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE TRENER';
    END IF;
END;
/
--
PROMPT
PROMPT *** TRENER table has been dropped
PROMPT
--

--
-- STADION --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'STADION';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE STADION';
    END IF;
END;
/
--
PROMPT
PROMPT *** STADION table has been dropped
PROMPT
--

--
-- SEZON --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'SEZON';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE SEZON';
    END IF;
END;
/
--
PROMPT
PROMPT *** SEZON table has been dropped
PROMPT
--

--
-- RODZAJ_SEDZIEGO --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'RODZAJ_SEDZIEGO';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE RODZAJ_SEDZIEGO';
    END IF;
END;
/
--
PROMPT
PROMPT *** RODZAJ_SEDZIEGO table has been dropped
PROMPT
--

--
-- POZYCJA --
--
DECLARE cnt NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt FROM user_tables WHERE table_name = 'POZYCJA';
    IF cnt <> 0 THEN
        EXECUTE IMMEDIATE 'DROP TABLE POZYCJA';
    END IF;
END;
/
--
PROMPT
PROMPT *** POZYCJA table has been dropped
PROMPT
--

--
PROMPT
PROMPT *** Rollback has been completed ***
PROMPT
--

EXIT;
