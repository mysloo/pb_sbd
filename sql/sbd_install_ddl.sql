-- #################################################################
--
-- Parameters:
--   1: DB SID
--   2: Owner user
--   3: Owner password
--
-- Usage:
--   sqlplus /nolog @sbd_install_ddl.sql <ORACLE_SID> <PRODUCT_USER> <PRODUCT_PASS>
--
--
--#################################################################
--
SET DEFINE ON
--
DEFINE db_sid  = &1
DEFINE own_usr = &2
DEFINE own_pwd = &3
--
CONNECT &own_usr/&own_pwd@&db_sid
--
--
SET FEEDBACK OFF
--
SET FEEDBACK ON
--

--
PROMPT
PROMPT *** TRENER table
PROMPT
--
create TABLE TRENER
(
	id 					NUMBER(2) 		NOT NULL 	PRIMARY KEY,
	imie 			    VARCHAR2(20),
	nazwisko            VARCHAR(30)
);

--
PROMPT
PROMPT *** DRUZYNA table
PROMPT
--
create TABLE DRUZYNA
(
    id                  NUMBER(2)       NOT NULL    PRIMARY KEY,
    nazwa               VARCHAR(50),
    trener_id           NUMBER(2)       NOT NULL,
    CONSTRAINT fk_druzyna_trener
    FOREIGN KEY (trener_id)
    REFERENCES TRENER(id)
);

--
PROMPT
PROMPT *** STADION table
PROMPT
--
CREATE TABLE STADION
(
    id                  NUMBER(2)       NOT NULL    PRIMARY KEY,
    nazwa               VARCHAR(50),
    lokalizacja         VARCHAR(100),
    pojemnosc           NUMBER(4)
);

--
PROMPT
PROMPT *** SEZON table
PROMPT
--
CREATE TABLE SEZON
(
    id                  NUMBER(2)       NOT NULL    PRIMARY KEY,
    nazwa               VARCHAR(20)
);

--
PROMPT
PROMPT *** RODZAJ_SEDZIEGO table
PROMPT
--
CREATE TABLE RODZAJ_SEDZIEGO
(
    id                  NUMBER(1)       NOT NULL    PRIMARY KEY,
    rodzaj              VARCHAR(30)
);

--
PROMPT
PROMPT *** SEDZIA table
PROMPT
--
CREATE TABLE SEDZIA
(
    id                  NUMBER(3)       NOT NULL    PRIMARY KEY,
    imie                VARCHAR(20),
    nazwisko            VARCHAR(30),
    rodzaj_sedziego_id  NUMBER(1)       NOT NULL,
    CONSTRAINT fk_sedzia_rodzaj_sedziego_id
    FOREIGN KEY (rodzaj_sedziego_id)
    REFERENCES RODZAJ_SEDZIEGO(id)
);

--
PROMPT
PROMPT *** MECZ table
PROMPT
--
CREATE TABLE MECZ
(
    id                  NUMBER(4)       NOT NULL    PRIMARY KEY,
    data                DATE,
    sezon_id            NUMBER(2)       NOT NULL,
    stadion_id          NUMBER(2)       NOT NULL,
    CONSTRAINT fk_mecz_sezon_id
    FOREIGN KEY (sezon_id)
    REFERENCES SEZON(id),
    CONSTRAINT fk_mecz_stadion_id
    FOREIGN KEY (stadion_id)
    REFERENCES STADION(id)
);

--
PROMPT
PROMPT *** LISTA_SEDZIOW table
PROMPT
--
CREATE TABLE LISTA_SEDZIOW
(
    id                  NUMBER(5)       NOT NULL    PRIMARY KEY,
    sedzia_id           NUMBER(3)       NOT NULL,
    mecz_id             NUMBER(4)       NOT NULL,
    CONSTRAINT fk_lista_sedziow_sedzia_id
    FOREIGN KEY (sedzia_id)
    REFERENCES SEDZIA(id),
    CONSTRAINT fk_lista_sedziow_mecz_id
    FOREIGN KEY (mecz_id)
    REFERENCES MECZ(id)
);

--
PROMPT
PROMPT *** POZYCJA table
PROMPT
--
CREATE TABLE POZYCJA
(
    id                  NUMBER(2)       NOT NULL    PRIMARY KEY,
    nazwa               VARCHAR(20)
);

--
PROMPT
PROMPT *** ZAWODNIK table
PROMPT
--
CREATE TABLE ZAWODNIK
(
    id                  NUMBER(4)       NOT NULL    PRIMARY KEY,
    imie                VARCHAR(20),
    nazwisko            VARCHAR(30),
    numer               NUMBER(2),
    data_urodzenia      DATE,
    pozycja_id          NUMBER(2)       NOT NULL,
    druzyna_id          NUMBER(2)       NOT NULL,
    CONSTRAINT fk_zawodnik_pozycja_id
    FOREIGN KEY (pozycja_id)
    REFERENCES POZYCJA(id),
    CONSTRAINT fk_zawodnik_druzyna_id
    FOREIGN KEY (druzyna_id)
    REFERENCES DRUZYNA(id)
);

--
PROMPT
PROMPT *** GOL table
PROMPT
--
CREATE TABLE GOL
(
    id                  NUMBER(5)       NOT NULL    PRIMARY KEY,
    mecz_id             NUMBER(4)       NOT NULL,
    zawodnik_id         NUMBER(4)       NOT NULL,
    ilosc               NUMBER(2),
    CONSTRAINT fk_gol_mecz_id
    FOREIGN KEY (mecz_id)
    REFERENCES MECZ(id),
    CONSTRAINT fk_gol_zawodnik_id
    FOREIGN KEY (zawodnik_id)
    REFERENCES ZAWODNIK(id)
);

--
PROMPT
PROMPT *** KARTKA table
PROMPT
--
CREATE TABLE KARTKA
(
    id                  NUMBER(5)       NOT NULL    PRIMARY KEY,
    rodzaj              VARCHAR(20),
    minuta_meczu        NUMBER(3),
    mecz_id             NUMBER(4)       NOT NULL,
    sedzia_id           NUMBER(3)       NOT NULL,
    zawodnik_id         NUMBER(4)       NOT NULL,
    CONSTRAINT fk_kartka_mecz_id
    FOREIGN KEY (mecz_id)
    REFERENCES MECZ(id),
    CONSTRAINT fk_kartka_sedzia_id
    FOREIGN KEY (sedzia_id)
    REFERENCES SEDZIA(id),
    CONSTRAINT fk_kartka_zawodnik_id
    FOREIGN KEY (zawodnik_id)
    REFERENCES ZAWODNIK(id)
);

--
PROMPT
PROMPT *** TRANSFER table
PROMPT
--
CREATE TABLE TRANSFER
(
    id                  NUMBER(5)       NOT NULL    PRIMARY KEY,
    data                DATE,
    cena                NUMBER(7),
    zawodnik_id         NUMBER(4)       NOT NULL,
    CONSTRAINT fk_transfer_zawodnik_id
    FOREIGN KEY (zawodnik_id)
    REFERENCES ZAWODNIK(id)
);

--
PROMPT
PROMPT *** LISTA_TRANSFEROW table
PROMPT
--
CREATE TABLE LISTA_TRANSFEROW
(
    id                  NUMBER(5)       NOT NULL    PRIMARY KEY,
    druzyna_id          NUMBER(2)       NOT NULL,
    transfer_id         NUMBER(5)       NOT NULL,
    CONSTRAINT fk_list_trans_druzyna_id
    FOREIGN KEY (druzyna_id)
    REFERENCES DRUZYNA(id),
    CONSTRAINT fk_list_trans_transfer_id
    FOREIGN KEY (transfer_id)
    REFERENCES TRANSFER(id)
);


--
PROMPT
PROMPT *** Installation has been completed ***
PROMPT
--
EXIT;
