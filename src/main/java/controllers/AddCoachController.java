package controllers;

import db.Database;
import db.tables.Trener;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class AddCoachController {
    private final Database database = Database.getInstance();


    @FXML
    private TextField nameTextField;
    @FXML
    private TextField surnameTextField;

    @FXML
    public void initialize() {

    }


    @FXML
    public void addCoach() {
        database.executeQuery((session) -> {
            session.save(new Trener(nameTextField.getText(), surnameTextField.getText()));

        });
    }
}
