package db;

import org.hibernate.Session;

public interface Executable {

    void execute(Session session);
}
