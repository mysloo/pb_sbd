package db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Database {
    private static Database ourInstance = new Database();

    private final SessionFactory sessionFactory; // A SessionFactory is set up once for an application!

    private Database() {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
    }


    public void executeQuery(Executable ex) {
        Session session = createSession();
        session.beginTransaction();
        ex.execute(session);
        session.getTransaction().commit();
        session.close();
    }

    private Session createSession() {
        return sessionFactory.openSession();
    }

    public static Database getInstance() {
        return ourInstance;
    }

}
