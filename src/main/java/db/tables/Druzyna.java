package db.tables;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "DRUZYNA")
public class Druzyna {
    @Id
    @GenericGenerator(name="simple_generator" , strategy="increment")
    @GeneratedValue(generator="simple_generator")
    @Column(name = "id", nullable = false)
    private Integer id;
    private String nazwa;
    @ManyToOne
    @JoinColumn(name = "trener_id")
    private Trener trener_id;

    public Druzyna() { }

    public Druzyna(String nazwa, Trener trener_id) {
        this.nazwa = nazwa;
        this.trener_id = trener_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Trener getTrener_id() {
        return trener_id;
    }

    public void setTrener_id(Trener trener_id) {
        this.trener_id = trener_id;
    }
}
