package db.tables;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "TRENER")
public class Trener {
    @Id
    @GenericGenerator(name="simple_generator" , strategy="increment")
    @GeneratedValue(generator="simple_generator")
    @Column(name = "id", nullable = false)
    private Integer id;
    private String imie;
    private String nazwisko;

    public Trener() { }

    public Trener(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
