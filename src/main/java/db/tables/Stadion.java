package db.tables;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "STADION")
public class Stadion {
    @Id
    @GenericGenerator(name="simple_generator" , strategy="increment")
    @GeneratedValue(generator="simple_generator")
    @Column(name = "id", nullable = false)
    private Integer id;
    private String stadion;
    private String lokalizacja;
    private Integer pojemnosc;

    public Stadion() { }

    public Stadion(String stadion, String lokalizacja, Integer pojemnosc) {
        this.stadion = stadion;
        this.lokalizacja = lokalizacja;
        this.pojemnosc = pojemnosc;
    }

    public String getStadion() {
        return stadion;
    }

    public void setStadion(String stadion) {
        this.stadion = stadion;
    }

    public String getLokalizacja() {
        return lokalizacja;
    }

    public void setLokalizacja(String lokalizacja) {
        this.lokalizacja = lokalizacja;
    }

    public Integer getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(Integer pojemnosc) {
        this.pojemnosc = pojemnosc;
    }
}
